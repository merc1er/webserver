# First steps

```
sudo su

apt-get install apache2 
apt-get install php
apt-get install libapache2-mod-php
```

#### enable htaccess
```
vim /etc/apache2/apache2.conf
```

#### phpmyadmin link location
```
sudo ln -s /usr/share/phpmyadmin/ /var/www/html/phpma
```

#### mbstring installation
```
sudo apt-get install phpmyadmin php-mbstring
sudo service apache2 restart
```

#### store git credential
```
git config credential.helper store
```