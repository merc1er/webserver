export PS1="\[\033[1;32m\]\u: \[\033[1;34m\]\w $ \[\033[0m\]"
# Enable colors
export CLICOLOR=1
force_color_prompt=yes

# Shortcuts
alias l='ls -la'

